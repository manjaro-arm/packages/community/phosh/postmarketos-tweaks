# Maintainer: Mark Wagie <mark at manjaro dot org>
# Maintainer: Philip Müller <philm at manjaro dot org>
# Contributor: Danct12 <danct12 at disroot dot org>

pkgname=('postmarketos-tweaks' 'postmarketos-tweaks-phosh' 'postmarketos-tweaks-sxmo')
pkgbase=postmarketos-tweaks
pkgver=0.13.2
pkgrel=1
pkgdesc="Application for exposing extra settings easily on mobile platforms"
arch=('any')
url="https://gitlab.com/postmarketOS/postmarketos-tweaks"
license=('GPL3')
depends=('gtk3' 'libhandy' 'python' 'python-gobject' 'python-requests' 'python-yaml')
makedepends=('git' 'meson')
checkdepends=('appstream')
_commit=8c561a8534a1416dda2ad218538d6baeca4c3eb1  # tags/0.13.2^0
source=("git+https://gitlab.com/postmarketOS/postmarketos-tweaks.git#commit=$_commit")
sha256sums=('9bd1f3190381a9d6cc3c72197f325273c069ab43c3356fbe817bdc3decc62952')

pkgver() {
    cd "$pkgbase"
    git describe --tags | sed 's/^v//;s/_//;s/-/+/g'
}

build() {
    arch-meson "$pkgbase" build
    meson compile -C build
}

check() {
    cd "$pkgbase"
    appstreamcli validate --no-net data/org.postmarketos.Tweaks.metainfo.xml || :
    desktop-file-validate data/org.postmarketos.Tweaks.desktop
}

package_postmarketos-tweaks() {
    conflicts=('postmarketos-tweaks-app')
    replaces=('postmarketos-tweaks-app')

    meson install -C build --destdir "$pkgdir"

    mv "$pkgdir/usr/share/$pkgbase/phosh.yml" "$srcdir"/
    mv "$pkgdir/usr/share/$pkgbase/sxmo-x11.yml" "$srcdir"/
}

package_postmarketos-tweaks-phosh() {
    pkgdesc+=" (Phosh support)"
    depends=('postmarketos-tweaks')
    conflicts=('postmarketos-tweaks-app-phosh')
    replaces=('postmarketos-tweaks-app-phosh')

    install -Dm644 phosh.yml -t "$pkgdir/usr/share/$pkgbase/"
}

package_postmarketos-tweaks-sxmo() {
    pkgdesc+=" (SXMO support)"
    depends=('postmarketos-tweaks')

    install -Dm644 sxmo-x11.yml -t "$pkgdir/usr/share/$pkgbase/"
}
